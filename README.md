# API Movies

<a href="https://API-Movies.gitlab.io"><img src="https://API-Movies.gitlab.io/banner.png"></a>

### The movie/tv statistics in graphs:

- https://API-Movies.gitlab.io

### Finding inconsistencies in the database:

- https://API-Movies.gitlab.io/test.html

### The movie/tv API lists:

- https://github.com/API-Movies/collaps
- https://github.com/API-Movies/alloha
- https://github.com/API-Movies/hdvb
- https://github.com/API-Movies/videocdn
- https://github.com/API-Movies/kodik
- https://github.com/API-Movies/bazon

###### MIT License
